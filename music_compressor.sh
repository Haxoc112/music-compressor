#!/bin/bash

## Specifies how to use the program if user does -h or --help
USAGE="$(basename "$0") is a simple music compressor that uses lame

All arguments are optional (because they have defaults):

-o|--disregard to compress all ( *.mp3 ) files in the Parent Directory, default is false
-c|--include-compressed to compress songs previously compressed with the same quality, default is false
-q|--quality	quality_in_kbps sets the quality of the compressed songs, must be between 16kbps and 320kbps the number only! default is 128
-p|--parent-directory	absolute_path_to_where_music_is sets the directory from which the program will work, must be absolute! default is /home/$USER/Music
-d|--directory relative_path_to_dir_from_from_parent sets the directory where the compressed songs will go, must be relative to parent directory, default is 'compressed_QUALITY'
-e|--exclude-list absolute_path_to_file sets the location of the list of songs user want excluded from the compression, must be a text file!, must be absolute! default is /dev/null
-l|--compressed-list relative_from_compressed_dir sets the location of the file to store a list of all compressed songs with same quality, is a relative path from compressed directory, default is music_compressed_list_QUALITY.txt
-h|--help displays this menu

"





## Initiate all variables:
DISREGARD=n ## Should the program include all songs, even if they are in the excluded list?
INCLUDE_COMPRESSED=n ## Should the program recompress files that were compressed with the same quality before?
QUALITY=128 ## Quality of the songs, 32 = 32kbps, 64 = 64kbps etc.. defaults to 128kbps
PARENT_DIRECTORY="/home/$USER/Music" ## What directory to work in where the music is, defaults to "/home/$USER/Music"
COMPRESSED_DIRECTORY="compressed_$QUALITY" ## Name is the directory to put compressed songs in, will always be a subdirectory of $PARENT_DIRECTORY
COMPRESSED_LIST="music_compressed_list_$QUALITY.txt" ## What name for the file which contains all the song names, always be inside $COMPRESSED_DIRECTORY
EXCLUDE_LIST=/dev/null ## List of songs to exclude, defaults to none



## Check if lame is installed
function check_lame {
	hash lame 2>/dev/null || { 
		echo -e >&2 "lame is required but not installed.\nDownload at http://lame.sf.net.\nAborting..."; 
		exit 1; 
	}
}

function get_args {
	# saner programming env: these switches turn some bugs into errors
	set -o errexit -o pipefail -o noclobber -o nounset

	! getopt --test > /dev/null 
	if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
		  echo "I’m sorry, `getopt --test` failed in this environment."
		  exit 2
	fi

	OPTIONS=:o:c:q:d:p:e:l:
	LONGOPTS=disregard:,include-compressed:,quality:,directory:,parent-directory:,exclude-list:,compressed-list:

	# -use ! and PIPESTATUS to get exit code with errexit set
	# -temporarily store output to be able to check for errors
	# -activate quoting/enhanced mode (e.g. by writing out “--options”)
	# -pass arguments only via   -- "$@"   to separate them correctly
	! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
	if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
		  # e.g. return value is 1
		  #  then getopt has complained about wrong arguments to stdout
		  exit 3
	fi
	# read getopt’s output this way to handle the quoting right:
	eval set -- "$PARSED"



	# Get arguments from commandline
	while true; do
		  case "$1" in
		      -o|--disregard)
		          DISREGARD=y
		          shift
		          ;;
		      -c|--include-compressed)
		          INCLUDE_COMPRESSED=y
		          shift
		          ;;
		      -q|--quality)
		          QUALITY="$2"
		          shift 2
		          ;;
		      -d|--directory)
		          COMPRESSED_DIRECTORY="$2"
		          shift 2
		          ;;
		      -p|--parent-directory)
		          PARENT_DIRECTORY="$2"
		          shift 2
		          ;;
		      -e|--exclude-list)
		          EXCLUDE_LIST="$2"
		          shift 2
		          ;;
		      -l|--compressed-list)
		          COMPRESSED_LIST="$2"
		          shift 2
		          ;;
		      -h|--help)
		          echo $USAGE
		          shift 2
		          exit
		          ;;		          
		      --)
		          shift
		          break
		          ;;
		      *)
		          echo "Programming error"
		          exit 4
		          ;;
		  esac
	done

	# handle non-option arguments
	#if [[ $# -ne 1 ]]; then
	#    echo "$0: A single input file is required."
	#    exit 5
	#fi
}

## Sets relative paths
function set_args {
	COMPRESSED_DIRECTORY="$PARENT_DIRECTORY/$COMPRESSED_DIRECTORY"
	COMPRESSED_LIST="$COMPRESSED_DIRECTORY/$COMPRESSED_LIST"
}


## Function to make sure file doesn't exist
function check_compressed_list {
	if [ ! -f "$COMPRESSED_LIST" ]; then
		touch "$COMPRESSED_LIST"
	else
		echo "file '$COMPRESSED_LIST' exists, will append"
	fi
}


## Checks that $EXCLUDE_LIST exists, aborts if not.
function check_excluded_list {
	## IF $EXCLUDE_LIST was changed from it's default value '/dev/null' and it doesn't exist, exit
	if [ ! "$EXCLUDE_LIST"="/dev/null" && ! -f "$EXCLUDE_LIST"]; then
		echo -e "File '$EXCLUDE_LIST' does not exist, please create or don't put in arguments \nAborting..."
		exit
}


## Make sure directries don't exist before trying to create them
function check_dir {
	## Iterate through input
	for folder in "$@"; do
		## IF doesn't folder exists
    if [ ! -d "$folder" ]; then
			mkdir -r "$folder"
		## Otherwise, folder does exist, notify user and quit
		else
			echo -e "folder '$folder' exists move or remove the folder.\nAborting..."
			exit
		fi
	done

}


## makes sure the quality argument is between the limits
function check_quality {
	## if $QUALITY less than 16kbps or greater than 320kbps, exit
	if [ "$QUALITY" -g 320 || "$QUALITY" -lt 16 ]; then
		echo -e "QUALITY exceed 320kbps or isn't larger than 16kbps \nAborting..."
		exit
	fi
}


## Song compress function uses 'lame'
function compress {
	## Compress song (first argument, $1) using lame with mp3input, outputting to $COMPRESSED_DIRECTORY
	lame --mp3input -b $QUALITY "$1" "$PARENT_DIRECTORY/$COMPRESSED_DIRECTORY/$1";
	## Adds song name (first argument, $1) to compressed list
	echo $(basename "$1") >> "$COMPRESSED_LIST"
}

## Skip song notification
function skip {
	echo "Skipping $1"
}

## This function checks the first arguments basename (without path) against the files that contain lists of file names 
## then decideds, based on user input (command line arguments) to compress or to skip
function check_song {
	## IF song is in excluded list ONLY
	if [ [ grep -Fxq $(basename "$1") "$EXCLUDE_LIST" ] && ! [ grep -Fxq $(basename "$1") "$COMPRESSED_LIST" ] ]; then
		## IF user chose to disregard any files compress song
		if [ $DISREGARD=y ]; then
			compress "$1"
		## Otherwise, skip song
		else
			skip "$1"
		fi
	
	## IF song in compressed list ONLY
	elif ! [ [ grep -Fxq $(basename "$1") "$EXCLUDE_LIST" ] && [ grep -Fxq $(basename "$1") "$COMPRESSED_LIST" ] ]; then
		## IF user told exclude compressed and didn't say to disregard files, skip song
		if [ $INCLUDE_COMPRESSED=n && $DISREGARD=n ]; then
			skip "$1"
		## Otherwise meaning user either said to include compressed, or said to disregard any files, compress song
		else
			compress "$1"
		fi
	
	## IF song is in both files
	elif [ [ grep -Fxq $(basename "$1") "$EXCLUDE_LIST" ] && [ grep -Fxq $(basename "$1") "$COMPRESSED_LIST" ] ]; then
		## IF user chose to disregard any files compress song
		if [ $DISREGARD=y ]; then
			compress "$1"
		## Otherwise, skip song
		else
			skip "$1"
		fi
	
	## song is in neither
	else
		compress "$1"
	fi
}

function main_loop {
	## Loops through every file, in $PARENT_DIRECTORY and all files in all subdirectories of $PARENT_DIRECTORY, that end with '.mp3'
	for SONG in "$PARENT_DIRECTORY/**.mp3"; do
		## Calls check song to make check if file is eligible for compression
		check_song "$SONG"
	done
}


function run {
	check_lame
	get_args
	set_vars
	check_compressed_list
	check_exclude_list
	check_dir "$PARENT_DIRECTORY" "$COMPRESSED_DIRECTORY"
	check_quality
	
	main_loop
}


echo "DISREGARD -> $DISREGARD"
echo "INCLUDE_COMPRESSED -> $INCLUDE_COMPRESSED"
echo "QUALITY -> $QUALITY"
echo "PARENT_DIRECTORY -> $PARENT_DIRECTORY"
echo "COMPRESSED_DIRECTORY -> $COMPRESSED_DIRECTORY"
echo "COMPRESSED_LIST -> $COMPRESSED_LIST"
echo "EXCLUDE_LIST -> $EXCLUDE_LIST"

## Verifies with the users their choices
echo "Do you wish to continue?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) run; break;;
        No ) exit;;
    esac
done




